import { Photo } from ".";
import {Client } from './types.generated';
import { IConfig } from "./Config";

/**
 * Extension of generated client of slice api parameters.
 */
export class ExtendedClient extends Client {
    constructor(configuration: IConfig, baseUrl?: string, http?: { fetch(url: RequestInfo, init?: RequestInit): Promise<Response> }) {
        super(configuration, baseUrl, http);
    }

    /**
     * Get all available photos in page
     * @param id (optional) Filter by photo ID
     * @param albumId (optional) Filter by album ID
     * @return successful operation
     */
    getPhotoPage(page: number, pageSize: number ): Promise<Photo[]> {
        let url_ = this.baseUrl + "/photos?";
        url_ += "_page=" + encodeURIComponent("" + page) + "&";
        url_ += "_limit=" + encodeURIComponent("" + pageSize) + "&";
        url_ = url_.replace(/[?&]$/, "");

        let options_ = <RequestInit> {
            method: "GET",
            headers: {
                "Accept": "application/json"
            }
        };

        return this.transformOptions(options_).then(transformedOptions_ => {
            return this.http.fetch(url_, transformedOptions_);
        }).then((_response: Response) => {
            return this.processGetPhotos(_response);
        });
    }
}
