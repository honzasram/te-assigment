import { IConfig } from "./Config";

export class BaseClient {
  private config: IConfig;

  protected constructor(config: IConfig) {
    this.config = config;
  }

  protected transformOptions = async (options: RequestInit): Promise<RequestInit> => {
    // usually this is place where I inject auth token... 
    // options.headers = {
    //   ...options.headers,
    // };
    return Promise.resolve(options);
  };

  protected transformResult<Response, U>(url: string, response: Response, processor: (response: Response) => U) {
    return processor(response);
  }

  protected getBaseUrl(values: string, values2?: string): string {
    return values2 ?? values;
  }
}
