import { RootStore } from "../../app/store/rootStore";

export interface IConfig {
  getBaseUrl: () => string;
}

export class Config implements IConfig {
  store: RootStore;

  constructor(store: RootStore) {
    this.store = store;
  }

  getBaseUrl = () => {
    return getBaseUrl();
  }
}

function getBaseUrl(){
  switch(process.env.NODE_ENV){
    case "production":
      return process.env.API_URL || window.location.origin;
    default:
      return process.env.API_URL || "https://jsonplaceholder.typicode.com/";
  }
}