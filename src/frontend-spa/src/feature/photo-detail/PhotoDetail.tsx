import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { RootStoreContext } from '../../app';
import { Config } from '../../share/types';
import { Client, Photo } from '../../share/types/types.generated';
import Paper from '@material-ui/core/Paper';
import { Theme, WithStyles, withStyles, WithTheme, withTheme } from '@material-ui/core/styles';
import Skeleton from '@material-ui/lab/Skeleton';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

interface IParamTypes {
  id?: string
}

type Props = WithStyles<typeof styles> & WithTheme;

const PhotoDetailConponent : React.FC<Props> = (props) => {
  const rootStore = useContext(RootStoreContext);
  const api = new Client(new Config(rootStore));
  const history = useHistory();
  const [ photo, setPhoto ] = useState<Photo | undefined>();
  const { id }  = useParams<IParamTypes>();

  const handleBack = useCallback(()=>{
    history.push("/");
  }, [history])

  useEffect(() => {
    if(id){
      try {
        var photoId = parseInt(id);
        api.getPhoto(photoId)
          .then(photo => setPhoto(photo))
          .catch(e => console.error(e));
      } catch (error) {
        console.error(error);
      }
    }
  }, [id])

  return (
    <Paper elevation={4}>
      <Button variant='outlined' className={props.classes.backButton} onClick={handleBack}>
        Back
      </Button>
      <Grid container justify='center' alignItems='center' alignContent='center' direction='column'>
        <Grid item >
          <Grid container direction='row' alignItems='center' justify='center'>
            <Grid item>
              {photo && (<img src={photo?.url ?? ""} alt={photo?.title ?? ""}/>)}
              {photo === undefined && (<Skeleton width={"600px"} height={"600px"}/>)}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  )
}

const styles = (theme: Theme) => ({
  backButton:{
    padding: theme.spacing(1),
    margin: theme.spacing(1),
  }
})

export const PhotoDetail = withStyles(styles)(withTheme(PhotoDetailConponent))