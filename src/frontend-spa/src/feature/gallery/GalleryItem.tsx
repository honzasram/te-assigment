import React, { useCallback } from 'react';
import { useHistory } from 'react-router';
import { Photo } from '../../share/types';

interface IProps {
  photo: Photo
}

export const GalleryItem : React.FC<IProps> = (props) => {
  const history = useHistory();

  const handleClick = useCallback(() => {
    history.push(`/detail/${props.photo.id}`);
  }, [history, props.photo]);

  return (
    <img src={props.photo.thumbnailUrl ?? ""} width="150" height="150" alt={props.photo.title ?? ""} onClick={handleClick}/>
  )
};