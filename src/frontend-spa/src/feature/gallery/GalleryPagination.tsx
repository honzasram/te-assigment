import React, { useCallback, useContext } from 'react';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import Typography from '@material-ui/core/Typography';
import { observer } from 'mobx-react-lite';
import { RootStoreContext } from '../../app';


const GalleryPaginationComponent : React.FC = () => {  
  const rootStore = useContext(RootStoreContext);
  const store = rootStore.galleryStore;

  const handlePaginationClick = useCallback((value: number) => {
    let desiredPage = store.page + value;
    if(desiredPage < 0) desiredPage = 0;
    store.setPage(desiredPage);
  }, [store]);

  return (
    <Grid container justify='flex-end' alignItems='center' spacing={1}>
      <Grid item>
        <IconButton onClick={() => handlePaginationClick(-1)} disabled={store.page === 0}>
          <NavigateBeforeIcon/>
        </IconButton>
      </Grid>
      <Grid item>
        <Typography>Page: {store.page + 1}</Typography>
      </Grid>
      <Grid item>
        <IconButton onClick={() => handlePaginationClick(1)}>
          <NavigateNextIcon />
        </IconButton>
      </Grid>
    </Grid>
  )
}

export const GalleryPagination = observer(GalleryPaginationComponent);