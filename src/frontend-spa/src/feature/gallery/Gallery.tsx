import React, {useContext, useEffect } from 'react';
import clsx from 'clsx';
import { Theme, WithStyles, withStyles, WithTheme, withTheme } from '@material-ui/core/styles';
import { observer } from 'mobx-react-lite';
import { RootStoreContext } from '../../app';
import Grid from '@material-ui/core/Grid';
import Skeleton from '@material-ui/lab/Skeleton';
import Paper from '@material-ui/core/Paper';
import { Photo } from '../../share/types';
import { GalleryPagination } from './GalleryPagination';
import { GalleryItem } from './GalleryItem';

type Props = WithStyles<typeof styles> & WithTheme

const GalleryComponent : React.FC<Props> = (props) =>{
  const rootStore = useContext(RootStoreContext);
  const store = rootStore.galleryStore;

  useEffect(() => {
    store.getPhotos();
  }, [])

  return (
    <Paper elevation={4} >
      <Grid 
        className={props.classes.content}
        container 
        spacing={4} 
        direction='row' 
        justify="center"
        alignItems="center"
      >
        {store.loading && generateLoadingSkeleton()}
        {!store.loading && generateItems(store.photos)}
      </Grid>
      <div className={clsx(props.classes.bottom, props.classes.pagination)}>
        <GalleryPagination />
      </div>
    </Paper>
  )
}

const generateLoadingSkeleton = () => Array.from({ length:10 }, (_, i) => i).map((_, i) => (
  <Grid item key={i}>
    <Skeleton key={`skeleton-${i}`} variant="rect" width={150} height={150} />
  </Grid>
));

const generateItems = (photos: Photo[]) => photos.map((photo, index) => (
  <Grid item key={`item-${photo.id}-${index}`}>
    <GalleryItem photo={photo}/>
  </Grid>
))

const styles = (theme: Theme) => ({
  root:{
    display: 'flex',
    flexDirection: 'column' as 'column',
  },
  image:{
    width: "150px",
    height: "150px",
  },
  pagination:{
    margin: theme.spacing(2)
  },
  content:{
    flex: 1,
    flexGrow: 1,
    padding: theme.spacing(2)
  },
  bottom:{
    flex: 0,
  }
})

export const Gallery = withStyles(styles)(withTheme(observer(GalleryComponent)));