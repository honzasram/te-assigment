import React, { useMemo } from 'react';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { DefaultLayout } from './layout/DefaultLayout';
import { NotFound } from './layout/NotFound';
import { Gallery, PhotoDetail } from '../feature';

export const App : React.FC = () => {
  const theme = useMemo(() => createMuiTheme({
    palette: {
      type: 'dark'
    }
  }), []);

  return (
    <MuiThemeProvider theme={theme} >
      <CssBaseline />
      <Router>
        <Switch>
          <Route path="/detail/:id" >
            <DefaultLayout>
              <PhotoDetail />
            </DefaultLayout>
          </Route>
          <Route exact path="/" >
            <DefaultLayout>
              <Gallery />
            </DefaultLayout>
          </Route>
          <Route component={NotFound}/>
        </Switch>
      </Router>
    </MuiThemeProvider>
  )
};