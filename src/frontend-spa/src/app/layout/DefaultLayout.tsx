import Container from '@material-ui/core/Container';
import React from 'react';


interface IProps {
  children: React.ReactChild | React.ReactChild[] 
}

export const DefaultLayout : React.FC<IProps> = (props) =>{
  return (
    <Container maxWidth={"md"}>
      {props.children}
    </Container>
  )
}