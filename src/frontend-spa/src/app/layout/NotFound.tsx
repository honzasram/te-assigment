import React, { CSSProperties } from 'react';
import { Theme, WithStyles, withStyles, WithTheme, withTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';

type Props = WithStyles<typeof styles> & WithTheme;

const NotFoundComponent : React.FC<Props> = (props) => {
  return (
    <Grid container justify='center' alignItems='center' alignContent='center' direction='column' className={props.classes.fullHeight}>
      <Grid item>
        <img src="https://media1.tenor.com/images/963f7cb24a851b5cb7604cdc75eb308a/tenor.gif?itemid=5120778" width="683" height="386.7590361445783" alt="Please Disperse GIF - Please Disperse Explosions GIFs"/>
      </Grid>
      <Grid item >
        <Grid container direction='row' alignItems='center' justify='center'>
          <Grid item>
            <Typography align='center'>
              Nothing here! Are you sure you want to be here?
            </Typography>
            <Link to='/'>
              <Typography variant='h6' align='center' className={props.classes.linkFont}>
                Go back to home...
              </Typography>
            </Link>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

const styles = (theme: Theme) => ({
  fullHeight: {
    height: '100vh'
  }, 
  linkFont:{
    color: theme.palette.text.primary,
    textDecorationColor: theme.palette.text.primary,
  },
})

export const NotFound = withStyles(styles)(withTheme(NotFoundComponent))