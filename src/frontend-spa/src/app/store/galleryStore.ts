import { action, makeObservable, observable, reaction, runInAction } from 'mobx';
import { RootStore } from './rootStore';
import { ExtendedClient, Config, Photo } from '../../share/types';

export class GalleryStore {
  rootStore : RootStore;
  api: ExtendedClient;

  constructor(rootStore : RootStore) {
    makeObservable(this)
    this.rootStore = rootStore;
    this.api = new ExtendedClient(new Config(rootStore));

    reaction(() => this.page, (next) => {
      this.getPhotos();
    });
    reaction(() => this.pageSize, (next) => {
      this.getPhotos();
    });
  }

  @observable loading : boolean = false;
  @observable photos : Photo[] = [];
  @observable page: number = 0;
  @observable pageSize: number = 10;

  @action getPhotos = async () => {
    try {
      runInAction(() => this.loading = true);
      var response = await this.api.getPhotoPage(this.page, this.pageSize);      
      if(response) {
        runInAction(() => this.photos = response);
      }
    }
    catch (error) {
      console.error(error);
    }
    finally {
      runInAction(() => this.loading = false);
    }
  }

  @action setPage = (page: number) => {
    runInAction(() => this.page = page );
  }

  @action setPageSize = (pageSize: number) => {
    runInAction(() => this.pageSize = pageSize );
  }
}
