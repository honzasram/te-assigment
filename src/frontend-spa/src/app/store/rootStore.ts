import { createContext } from 'react';
import { configure } from 'mobx';
import { GalleryStore } from './galleryStore';
 
configure({enforceActions: 'always'});

export class RootStore {
  galleryStore: GalleryStore;

  constructor() {
    this.galleryStore = new GalleryStore(this);
  }
}

export const RootStoreContext = createContext(new RootStore());