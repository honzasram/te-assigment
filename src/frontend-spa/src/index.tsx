import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './app';

const Root : React.FC = () => <App />;

ReactDOM.render(<Root />, document.getElementById('root'));
